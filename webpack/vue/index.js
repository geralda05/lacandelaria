import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import index from './views/index.vue'
import inicio from './views/templates/inicio/inicio.vue'
import contacto from './views/templates/inicio/contacto.vue'
import historia from './views/templates/inicio/historia.vue'
import instalaciones from './views/templates/inicio/instalaciones.vue'
import login from './views/templates/inicio/login.vue'
import dashboard from './views/templates/dashboard/dashboard.vue'
import usuarios from './views/templates/dashboard/users.vue'
import home from './views/templates/dashboard/home.vue'
import routerView from './views/templates/dashboard/routerView.vue'
import secciones from './views/templates/dashboard/secciones.vue'
import seccion from './views/templates/dashboard/seccion.vue'
import periodos from './views/templates/dashboard/periodos.vue'
import nuevoalumno from './views/templates/dashboard/nuevoalumno.vue'
import modificaralumno from './views/templates/dashboard/modificaralumno.vue'
import inscripcion from './views/templates/dashboard/inscripcion.vue'
import docentes from './views/templates/dashboard/docentes.vue'
import alumnos from './views/templates/dashboard/alumnos.vue'

const router = new VueRouter({
    mode:'history',
    routes:[
        {
            path:'/',
            redirect: "/inicio",

        },
        {
            path:'/inicio',
            name:"inicio",
            component: inicio,
        },
        {
            path:'/historia',
            name:"historia",
            component: historia,
        },
        {
            path:'/contacto',
            name:"contacto",
            component: contacto,
        },
        {
            path:'/instalaciones',
            name:"instalaciones",
            component: instalaciones,
        },
        {
            path:'/login',
            name:"login",
            component: login,
        },
        {
            path:'/dashboard',
            name:"dashboard",
            component: dashboard,
            beforeEnter:(to, from, next) => {
                if (localStorage.login_sisacademico){
                    next();
                }else{
                    next('/login'); 
                } 
            },
            children:[
                {
                    path:'/',
                    name:"home",
                    component:home,
                },
                {
                    path:'alumnos',
                    name:"routerView",
                    component: routerView,
                    props:true,
                    beforeEnter:(to, from, next) => {
                        if (localStorage.login_sisacademico){
                            next();
                        }else{
                            next('/login'); 
                        } 
                    },
                    children:[
                        {
                            path:'/',
                            name:"alumnos",
                            component:alumnos,
                            beforeEnter:(to, from, next) => {
                                if (localStorage.login_sisacademico){
                                    next();
                                }else{
                                    next('/login'); 
                                } 
                            },
                        },
                        {
                            path:'nuevo',
                            name:"nuevoalumno",
                            component: nuevoalumno,
                            beforeEnter:(to, from, next) => {
                                if (localStorage.login_sisacademico){
                                    next();
                                }else{
                                    next('/login'); 
                                } 
                            },
                        },
                        {
                            path:'modificar/:AlumnoID',
                            name:"modificaralumno",
                            component: modificaralumno,
                            props:true,
                            beforeEnter:(to, from, next) => {
                                if (localStorage.login_sisacademico){
                                    next();
                                }else{
                                    next('/login'); 
                                } 
                            },
                        },
                    ]
                },
                {
                    path:'docentes',
                    name:"docentes",
                    component: docentes,
                    beforeEnter:(to, from, next) => {
                        if (localStorage.login_sisacademico){
                            next();
                        }else{
                            next('/login'); 
                        } 
                    },
                },
                {
                    path:'inscripcion',
                    name:"inscripcion",
                    component: inscripcion,
                    beforeEnter:(to, from, next) => {
                        if (localStorage.login_sisacademico){
                            next();
                        }else{
                            next('/login'); 
                        } 
                    },
                },
                {
                    path:'periodos',
                    name:"periodos",
                    component: periodos,
                    beforeEnter:(to, from, next) => {
                        if (localStorage.login_sisacademico){
                            next();
                        }else{
                            next('/login'); 
                        } 
                    },
                },
                {
                    path:'secciones',
                    name:"secciones",
                    component: secciones,
                    beforeEnter:(to, from, next) => {
                        if (localStorage.login_sisacademico){
                            next();
                        }else{
                            next('/login'); 
                        } 
                    },
                },
                {
                    path:'seccion',
                    name:"seccion",
                    component: seccion,
                    beforeEnter:(to, from, next) => {
                        if (localStorage.login_sisacademico){
                            next();
                        }else{
                            next('/login'); 
                        } 
                    },
                },
                {
                    path:'usuarios',
                    name:"usuarios",
                    component: usuarios,
                    beforeEnter:(to, from, next) => {
                        if (localStorage.login_sisacademico){
                            next();
                        }else{
                            next('/login'); 
                        } 
                    },
                }        
            ]
        },
        {
            path:"*",
            redirect:"/inicio",
        }
    ]
});

const app = new Vue({
    router,
    render: createEle => createEle(index)

}).$mount('#app');