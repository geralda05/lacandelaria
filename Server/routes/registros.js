var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var collector = express();

collector.use(bodyParser.urlencoded({
  extended:true,
}))

collector.use(bodyParser.json());
collector.use(cookieParser());
collector.use(express.static(path.join(__dirname, 'public')));

/* Conexión de la BD */
var connections = require('../configs/connection');

/* Leer Registros de la BD */
collector.get('/leerAlumnos/:AlumnoCedula?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.AlumnoCedula ? "%"+req.params.AlumnoCedula+"%":null),
            (req.params.AlumnoCedula ? "%"+req.params.AlumnoCedula+"%":null),
            (req.params.AlumnoCedula ? "%"+req.params.AlumnoCedula+"%":null),
            (req.params.AlumnoCedula ? "%"+req.params.AlumnoCedula+"%":null),
        ]
        var SQL = (req.params.AlumnoCedula ? "SELECT * FROM alumnos WHERE AlumnoCedula LIKE ? OR AlumnoFname LIKE ? OR AlumnoLname LIKE ? OR AlumnoLLname LIKE ?":"SELECT * FROM alumnos");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/leerPadres/:AlumnoCedula?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.AlumnoCedula ? "%"+req.params.AlumnoCedula+"%":null),
            (req.params.AlumnoCedula ? "%"+req.params.AlumnoCedula+"%":null),
            (req.params.AlumnoCedula ? "%"+req.params.AlumnoCedula+"%":null),
            (req.params.AlumnoCedula ? "%"+req.params.AlumnoCedula+"%":null),
        ]
        var SQL = (req.params.AlumnoCedula ? "SELECT * FROM alumnos WHERE AlumnoRepresentanteCedula LIKE ? OR AlumnoFname LIKE ? OR AlumnoPadreCedula LIKE ? OR AlumnoROtroCedula LIKE ?":"SELECT * FROM alumnos");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/leerUsuarios/:DocenteEmail?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            req.body.DocenteEmail,
        ]
        var SQL = (req.body.DocenteEmail ? "SELECT * FROM docentes WHERE DocenteEmail LIKE ?":"SELECT * FROM alumnos");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/buscarAlumnos/:AlumnoCedula?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.AlumnoCedula ? req.params.AlumnoCedula:null),
        ]
        var SQL = (req.params.AlumnoCedula ? "SELECT * FROM alumnos WHERE AlumnoCedula=? ":"SELECT * FROM alumnos");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/leerDocentes/:DocenteCedula?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.DocenteCedula ? req.params.DocenteCedula:null),
        ]
        var SQL = (req.params.DocenteCedula ? "SELECT * FROM docentes WHERE DocenteCedula=? ":"SELECT * FROM docentes");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/leerSecciones/:SeccionID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.SeccionID ? req.params.SeccionID:null),
        ]
        var SQL = (req.params.SeccionID ? "SELECT * FROM secciones WHERE SeccionID=?":"SELECT * FROM secciones");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/leerInscripciones/:InscripcionEstudianteID?/:InscripcionPeriodoID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.InscripcionEstudianteID ? req.params.InscripcionEstudianteID:null),
            (req.params.InscripcionPeriodoID ? req.params.InscripcionPeriodoID:null)
        ]
        var SQL = (req.params.InscripcionEstudianteID ? "SELECT * FROM inscripciones WHERE InscripcionEstudianteID=? AND InscripcionPeriodoID=?":"SELECT * FROM inscripciones");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/buscarInscripciones/:InscripcionEstudianteID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.InscripcionEstudianteID ? req.params.InscripcionEstudianteID:null),
        ]
        var SQL = (req.params.InscripcionEstudianteID ? "SELECT * FROM inscripciones WHERE InscripcionEstudianteID=?":"SELECT * FROM inscripciones");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/leerNominas/:InscripcionPeriodoID?/:InscripcionSeccionID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.InscripcionSeccionID ? req.params.InscripcionSeccionID:null),
            (req.params.InscripcionPeriodoID ? req.params.InscripcionPeriodoID:null)
        ]
        var SQL = (req.params.InscripcionSeccionID ? "SELECT * FROM inscripciones WHERE InscripcionSeccionID=? AND InscripcionPeriodoID=?":"SELECT * FROM inscripciones");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/leerPeriodos/:PeriodoID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.PeriodoID ? req.params.PeriodoID:null),
        ]
        var SQL = (req.params.PeriodoID ? "SELECT * FROM periodos WHERE PeriodoID=?":"SELECT * FROM periodos");
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})
/* Función de Agregar a la BD */
collector.post('/AgregarAlumno',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.AlumnoCedula !== 'undefined'){

        let Request = [
            req.body.AlumnoFname,
            req.body.AlumnoMname,
            req.body.AlumnoLname,
            req.body.AlumnoLLname,
            req.body.AlumnoCedula,                            
            req.body.AlumnoDireccion,
            req.body.AlumnoGenero,
            req.body.AlumnoFnacimiento,
            req.body.AlumnoRepresentanteNombre,
            req.body.AlumnoRepresentanteCedula,
            req.body.AlumnoTelefono,
            req.body.AlumnoNacimiento,
            req.body.AlumnoTallaCamisa,
            req.body.AlumnoTallaPantalon,
            req.body.AlumnoTallaZapatos,
            req.body.AlumnoEstatura,
            req.body.AlumnoPeso,
            req.body.AlumnoAlergias,
            req.body.AlumnoEnfermedades,
            req.body.AlumnoPlantelProcedencia,
            req.body.AlumnoPlantelCodigo,
            req.body.AlumnoPlantelLocalidad,
            req.body.AlumnoCanaima,
            req.body.AlumnoCanaimaSerial,
            req.body.AlumnoCanaimaOperatividad,
            req.body.AlumnoCanaimaObservaciones,
            req.body.AlumnoLateralidad,
            req.body.AlumnoHermanosEstudiantes,
            req.body.AlumnoHermanosObservaciones,
            req.body.AlumnoBeca,
            req.body.AlumnoBecaNivel,
            req.body.AlumnoTipoVivienda,
            req.body.AlumnoCondicionVivienda,
            req.body.AlumnoCargaFamiliar,
            req.body.AlumnoNecesidades,
            req.body.AlumnoRepresentanteCivil,
            req.body.AlumnoRepresentanteNacimiento,
            req.body.AlumnoRepresentanteFnacimiento,
            req.body.AlumnoRepresentanteInstruccion,
            req.body.AlumnoRepresentanteOcupacion,
            req.body.AlumnoRepresentanteCel,
            req.body.AlumnoRepresentanteTelOtro,
            req.body.AlumnoRepresentanteDireccion,
            req.body.AlumnoPadreNombre,
            req.body.AlumnoPadreCedula,
            req.body.AlumnoPadreCivil,
            req.body.AlumnoPadreFnacimiento,
            req.body.AlumnoPadreNacimiento,
            req.body.AlumnoPadreDireccion,
            req.body.AlumnoPadreInstruccion,
            req.body.AlumnoPadreOcupacion,
            req.body.AlumnoPadreTel,
            req.body.AlumnoPadreCel,
            req.body.AlumnoPadreTelOtro,
            req.body.AlumnoROtroCedula,
            req.body.AlumnoROtroNombre,                            
            req.body.AlumnoROtroParentesco,
            req.body.AlumnoROtroCivil,
            req.body.AlumnoROtroFnacimiento,
            req.body.AlumnoROtroNacimiento,
            req.body.AlumnoROtroDireccion,
            req.body.AlumnoROtroInstruccion,
            req.body.AlumnoROtroOcupacion,
            req.body.AlumnoROtroTel,
            req.body.AlumnoROtroCel,
            req.body.AlumnoROtroTelOtro,
            req.body.AlumnoRepresentanteStatus,
            req.body.AlumnoPadreStatus,
            req.body.AlumnoROtroStatus
        ]
        var SQL = "INSERT INTO alumnos (AlumnoFname, AlumnoMname, AlumnoLname, AlumnoLLname, AlumnoCedula, AlumnoDireccion, AlumnoGenero, AlumnoFnacimiento, AlumnoRepresentanteNombre, AlumnoRepresentanteCedula, AlumnoTelefono, AlumnoNacimiento, AlumnoTallaCamisa, AlumnoTallaPantalon, AlumnoTallaZapatos, AlumnoEstatura, AlumnoPeso, AlumnoAlergias, AlumnoEnfermedades, AlumnoPlantelProcedencia, AlumnoPlantelCodigo, AlumnoPlantelLocalidad, AlumnoCanaima, AlumnoCanaimaSerial, AlumnoCanaimaOperatividad, AlumnoCanaimaObservaciones, AlumnoLateralidad, AlumnoHermanosEstudiantes, AlumnoHermanosObservaciones, AlumnoBeca, AlumnoBecaNivel, AlumnoTipoVivienda, AlumnoCondicionVivienda, AlumnoCargaFamiliar, AlumnoNecesidades, AlumnoRepresentanteCivil, AlumnoRepresentanteNacimiento, AlumnoRepresentanteFnacimiento, AlumnoRepresentanteInstruccion, AlumnoRepresentanteOcupacion, AlumnoRepresentanteCel, AlumnoRepresentanteTelOtro, AlumnoRepresentanteDireccion, AlumnoPadreNombre, AlumnoPadreCedula, AlumnoPadreCivil, AlumnoPadreFnacimiento, AlumnoPadreNacimiento, AlumnoPadreDireccion, AlumnoPadreInstruccion, AlumnoPadreOcupacion, AlumnoPadreTel, AlumnoPadreCel, AlumnoPadreTelOtro, AlumnoROtroCedula, AlumnoROtroNombre, AlumnoROtroParentesco, AlumnoROtroCivil, AlumnoROtroFnacimiento, AlumnoROtroNacimiento, AlumnoROtroDireccion, AlumnoROtroInstruccion, AlumnoROtroOcupacion, AlumnoROtroTel, AlumnoROtroCel, AlumnoROtroTelOtro, AlumnoRepresentanteStatus, AlumnoPadreStatus, AlumnoROtroStatus) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/AgregarDocente',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.DocenteCedula !== 'undefined'){

        let Request = [
            req.body.DocenteCedula,
            req.body.DocenteFname,
            req.body.DocenteMname,
            req.body.DocenteLname,
            req.body.DocenteLLname,                                   
            req.body.DocenteCargo,
            req.body.DocenteAutorizacion,
            req.body.DocenteEmail,
            req.body.DocentePassword
        ]
        var SQL = "INSERT INTO docentes (DocenteCedula,DocenteFname,DocenteMname,DocenteLname,DocenteLLname,DocenteCargo,DocenteAutorizacion,DocenteEmail,DocentePassword) VALUES (?,?,?,?,?,?,?,?,?)";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/inscribir',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.InscripcionEstudianteID !== 'undefined'){

        let Request = [
            req.body.InscripcionPeriodoID,
            req.body.InscripcionEstudianteID,
            req.body.InscripcionSeccionID
        ]
        var SQL = "INSERT INTO inscripciones (InscripcionPeriodoID,InscripcionEstudianteID,InscripcionSeccionID) VALUES (?,?,?)";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/agregarSeccion',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.SeccionTipo !== 'undefined'){

        let Request = [
            req.body.SeccionNivel,
            req.body.SeccionTipo,
            req.body.SeccionNombre
        ]
        var SQL = "INSERT INTO secciones (SeccionNivel,SeccionTipo,SeccionNombre) VALUES (?,?,?)";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/agregarPeriodo',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.PeriodoAnoInicial !== 'undefined'){

        let Request = [
            req.body.PeriodoAnoInicial,
            req.body.PeriodoAnoFinal
        ]
        var SQL = "INSERT INTO periodos (PeriodoAnoInicial,PeriodoAnoFinal) VALUES (?,?)";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Modificar en la BD */
collector.post('/modificar/:AlumnoID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.AlumnoCedula !== 'undefined'){

        let Request = [
            req.body.AlumnoFname,
            req.body.AlumnoMname,
            req.body.AlumnoLname,
            req.body.AlumnoLLname,
            req.body.AlumnoCedula,                            
            req.body.AlumnoDireccion,
            req.body.AlumnoGenero,
            req.body.AlumnoFnacimiento,
            req.body.AlumnoRepresentanteNombre,
            req.body.AlumnoRepresentanteCedula,
            req.body.AlumnoTelefono,
            req.body.AlumnoNacimiento,
            req.body.AlumnoTallaCamisa,
            req.body.AlumnoTallaPantalon,
            req.body.AlumnoTallaZapatos,
            req.body.AlumnoEstatura,
            req.body.AlumnoPeso,
            req.body.AlumnoAlergias,
            req.body.AlumnoEnfermedades,
            req.body.AlumnoPlantelProcedencia,
            req.body.AlumnoPlantelCodigo,
            req.body.AlumnoPlantelLocalidad,
            req.body.AlumnoCanaima,
            req.body.AlumnoCanaimaSerial,
            req.body.AlumnoCanaimaOperatividad,
            req.body.AlumnoCanaimaObservaciones,
            req.body.AlumnoLateralidad,
            req.body.AlumnoHermanosEstudiantes,
            req.body.AlumnoHermanosObservaciones,
            req.body.AlumnoBeca,
            req.body.AlumnoBecaNivel,
            req.body.AlumnoTipoVivienda,
            req.body.AlumnoCondicionVivienda,
            req.body.AlumnoCargaFamiliar,
            req.body.AlumnoNecesidades,
            req.body.AlumnoRepresentanteCivil,
            req.body.AlumnoRepresentanteNacimiento,
            req.body.AlumnoRepresentanteFnacimiento,
            req.body.AlumnoRepresentanteInstruccion,
            req.body.AlumnoRepresentanteOcupacion,
            req.body.AlumnoRepresentanteCel,
            req.body.AlumnoRepresentanteTelOtro,
            req.body.AlumnoRepresentanteDireccion,
            req.body.AlumnoPadreNombre,
            req.body.AlumnoPadreCedula,
            req.body.AlumnoPadreCivil,
            req.body.AlumnoPadreFnacimiento,
            req.body.AlumnoPadreNacimiento,
            req.body.AlumnoPadreDireccion,
            req.body.AlumnoPadreInstruccion,
            req.body.AlumnoPadreOcupacion,
            req.body.AlumnoPadreTel,
            req.body.AlumnoPadreCel,
            req.body.AlumnoPadreTelOtro,
            req.body.AlumnoROtroCedula,
            req.body.AlumnoROtroNombre,                            
            req.body.AlumnoROtroParentesco,
            req.body.AlumnoROtroCivil,
            req.body.AlumnoROtroFnacimiento,
            req.body.AlumnoROtroNacimiento,
            req.body.AlumnoROtroDireccion,
            req.body.AlumnoROtroInstruccion,
            req.body.AlumnoROtroOcupacion,
            req.body.AlumnoROtroTel,
            req.body.AlumnoROtroCel,
            req.body.AlumnoROtroTelOtro,
            req.body.AlumnoRepresentanteStatus,
            req.body.AlumnoPadreStatus,
            req.body.AlumnoROtroStatus
        ]
        var SQL = "UPDATE alumnos SET AlumnoFname=? , AlumnoMname=? , AlumnoLname=? , AlumnoLLname=? , AlumnoCedula=? , AlumnoDireccion=? , AlumnoGenero=? , AlumnoFnacimiento=? , AlumnoRepresentanteNombre=? , AlumnoRepresentanteCedula=? , AlumnoTelefono=? , AlumnoNacimiento=? , AlumnoTallaCamisa=? , AlumnoTallaPantalon=? , AlumnoTallaZapatos=? , AlumnoEstatura=? , AlumnoPeso=? , AlumnoAlergias=? , AlumnoEnfermedades=? , AlumnoPlantelProcedencia=? , AlumnoPlantelCodigo=? , AlumnoPlantelLocalidad=? , AlumnoCanaima=? , AlumnoCanaimaSerial=? , AlumnoCanaimaOperatividad=? , AlumnoCanaimaObservaciones=? , AlumnoLateralidad=? , AlumnoHermanosEstudiantes=? , AlumnoHermanosObservaciones=? , AlumnoBeca=? , AlumnoBecaNivel=? , AlumnoTipoVivienda=? , AlumnoCondicionVivienda=? , AlumnoCargaFamiliar=? , AlumnoNecesidades=? , AlumnoRepresentanteCivil=? , AlumnoRepresentanteNacimiento=? , AlumnoRepresentanteFnacimiento=? , AlumnoRepresentanteInstruccion=? , AlumnoRepresentanteOcupacion=? , AlumnoRepresentanteCel=? , AlumnoRepresentanteTelOtro=? , AlumnoRepresentanteDireccion=? , AlumnoPadreNombre=? , AlumnoPadreCedula=? , AlumnoPadreCivil=? , AlumnoPadreFnacimiento=? , AlumnoPadreNacimiento=? , AlumnoPadreDireccion=? , AlumnoPadreInstruccion=? , AlumnoPadreOcupacion=? , AlumnoPadreTel=? , AlumnoPadreCel=? , AlumnoPadreTelOtro=? , AlumnoROtroCedula=? , AlumnoROtroNombre=? , AlumnoROtroParentesco=? , AlumnoROtroCivil=? , AlumnoROtroFnacimiento=? , AlumnoROtroNacimiento=? , AlumnoROtroDireccion=? , AlumnoROtroInstruccion=? , AlumnoROtroOcupacion=? , AlumnoROtroTel=? , AlumnoROtroCel=? , AlumnoROtroTelOtro=? , AlumnoRepresentanteStatus=? , AlumnoPadreStatus=? , AlumnoROtroStatus=?  WHERE AlumnoID='"+req.params.AlumnoID+"'";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/modificarSecciones/:SeccionID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.SeccionNivel !== 'undefined'){

        let Request = [
            req.body.SeccionNivel,
            req.body.SeccionTipo,
            req.body.SeccionNombre
        ]
        var SQL = "UPDATE secciones SET SeccionNivel=? , SeccionTipo=?, SeccionNombre=? WHERE SeccionID='"+req.params.SeccionID+"'";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/modificarDocentes/:DocenteID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.DocenteCedula !== 'undefined'){

        let Request = [
            req.body.DocenteCedula,
            req.body.DocenteFname,
            req.body.DocenteMname,
            req.body.DocenteLname,
            req.body.DocenteLLname,
            req.body.DocenteCargo,
            req.body.DocenteAutorizacion, 
            req.body.DocenteEmail,
            req.body.DocentePassword           
        ]
        var SQL = "UPDATE docentes SET DocenteCedula=? , DocenteFname=?, DocenteMname=?, DocenteLname=?, DocenteLLname=?, DocenteCargo=?, DocenteAutorizacion=?, DocenteEmail=?, DocentePassword=? WHERE DocenteID='"+req.params.DocenteID+"'";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/modificarPeriodos/:PeriodoID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.PeriodoAnoInicial !== 'undefined'){

        let Request = [
            req.body.PeriodoAnoInicial,
            req.body.PeriodoAnoFinal
        ]
        var SQL = "UPDATE periodos SET PeriodoAnoInicial=? , PeriodoAnoFinal=? WHERE PeriodoID='"+req.params.PeriodoID+"'";
        connections.lacandelaria.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Eliminar en la BD */
collector.get('/eliminarUsuarios/:DocenteID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        var SQL = "DELETE FROM docentes WHERE DocenteID='"+req.params.DocenteID+"'";
        connections.lacandelaria.query(SQL,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/eliminarInscripcion/:InscripcionEstudianteID/:InscripcionPeriodoID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        var SQL = "DELETE FROM inscripciones WHERE InscripcionEstudianteID='"+req.params.InscripcionEstudianteID+"' AND InscripcionPeriodoID='"+req.params.InscripcionPeriodoID+"'";
        connections.lacandelaria.query(SQL,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

module.exports = collector;