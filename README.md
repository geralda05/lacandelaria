# SISTEMA DE INSCRIPCIONES PARA LA U.E.E."NUESTRA SEÑORA DE LA CANDELARIA"
## CREADO POR GERALD LEONEL ALARCON Y BENNY HURTADO

### PROBAR EN MODO DESARROLLADOR:

Instalar todas las dependencias ejecutando el comando “npm install” dentro de la carpeta Server y dentro de la carpeta Webpack. Una vez instaladas, debes ejecutar en consola “npm run webpack:watch” dentro de la carpeta Webpack, para que todos los cambios que hagas dentro de los templates de vue sean actualizados automáticamente en el archivo comprimido core.js ubicado en el servidor, el cual funciona al ejecutar simultáneamente en consola “npm start” dentro de la carpeta Server y una vez este corriendo el terminal podrás visualizar la interfaz dentro del puerto 3000.

## CREADO CON FINES DE TRABAJO ESPECIAL DE GRADO PARA OPTAR POR EL TITULO DE INGENIERO EN COMPUTACION


