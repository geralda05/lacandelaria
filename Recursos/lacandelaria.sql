-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-05-2019 a las 13:34:56
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lacandelaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `AlumnoID` int(11) NOT NULL,
  `AlumnoFname` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoMname` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoLname` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoLLname` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoCedula` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoDireccion` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoGenero` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoFNacimiento` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteNombre` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteCedula` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoTelefono` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoNacimiento` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoTallaCamisa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoTallaPantalon` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoTallaZapatos` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoEstatura` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPeso` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoAlergias` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoEnfermedades` varchar(400) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPlantelProcedencia` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPlantelCodigo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPlantelLocalidad` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoCanaima` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoCanaimaSerial` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoCanaimaOperatividad` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoCanaimaObservaciones` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoLateralidad` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoHermanosEstudiantes` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoHermanosObservaciones` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoBeca` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoBecaNivel` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoTipoVivienda` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoCondicionVivienda` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoCargaFamiliar` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoNecesidades` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteCivil` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteNacimiento` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteFnacimiento` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteInstruccion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteOcupacion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteCel` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteTelOtro` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteDireccion` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreNombre` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreCedula` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreCivil` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreFnacimiento` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreNacimiento` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreDireccion` varchar(400) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreInstruccion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreOcupacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreTel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreCel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreTelOtro` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroCedula` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroNombre` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroParentesco` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroCivil` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroFnacimiento` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroNacimiento` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroDireccion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroInstruccion` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroOcupacion` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroTel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroCel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroTelOtro` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoRepresentanteStatus` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoPadreStatus` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `AlumnoROtroStatus` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docentes`
--

CREATE TABLE `docentes` (
  `DocenteID` int(11) NOT NULL,
  `DocenteFname` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `DocenteMname` varchar(700) COLLATE utf8_spanish_ci NOT NULL,
  `DocenteLname` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `DocenteLLname` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `DocenteCedula` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `DocenteEmail` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `DocentePassword` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `DocenteAutorizacion` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `DocenteCargo` varchar(600) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `docentes`
--

INSERT INTO `docentes` (`DocenteID`, `DocenteFname`, `DocenteMname`, `DocenteLname`, `DocenteLLname`, `DocenteCedula`, `DocenteEmail`, `DocentePassword`, `DocenteAutorizacion`, `DocenteCargo`) VALUES
(24, 'Gerald', 'Leonel', 'Alarcon', '', 'V-27618428', 'gerald_leonel_5@hotmail.com', 'gerald07', 'true', 'Tecnico'),
(25, 'Roberto ', '', 'Di Michele', '', 'V-19499455', 'rdmichele@gmail.com', '12345678', 'true', 'Docente'),
(26, 'Benny', '', 'Hurtado', '', 'V-27070469', 'bennyjosue99@gmail.com', '12345678', 'true', 'Docente'),
(27, 'Freddy', '', 'Graterol', '', 'V-000000', 'freddyg@lc.com', '12345678', 'true', 'Docente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripciones`
--

CREATE TABLE `inscripciones` (
  `InscripcionID` int(11) NOT NULL,
  `InscripcionPeriodoID` int(11) NOT NULL,
  `InscripcionEstudianteID` int(11) NOT NULL,
  `InscripcionSeccionID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos`
--

CREATE TABLE `periodos` (
  `PeriodoID` int(11) NOT NULL,
  `PeriodoAnoInicial` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `PeriodoAnoFinal` varchar(600) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE `secciones` (
  `SeccionID` int(11) NOT NULL,
  `SeccionNivel` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `SeccionTipo` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `SeccionNombre` varchar(600) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`AlumnoID`);

--
-- Indices de la tabla `docentes`
--
ALTER TABLE `docentes`
  ADD PRIMARY KEY (`DocenteID`);

--
-- Indices de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD PRIMARY KEY (`InscripcionID`),
  ADD KEY `Asignar Campos` (`InscripcionEstudianteID`),
  ADD KEY `Asignar Periodo` (`InscripcionPeriodoID`),
  ADD KEY `Asignar Seccion` (`InscripcionSeccionID`);

--
-- Indices de la tabla `periodos`
--
ALTER TABLE `periodos`
  ADD PRIMARY KEY (`PeriodoID`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`SeccionID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `AlumnoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `docentes`
--
ALTER TABLE `docentes`
  MODIFY `DocenteID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  MODIFY `InscripcionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `periodos`
--
ALTER TABLE `periodos`
  MODIFY `PeriodoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `SeccionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD CONSTRAINT `Asignar Campos` FOREIGN KEY (`InscripcionEstudianteID`) REFERENCES `alumnos` (`AlumnoID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Asignar Periodo` FOREIGN KEY (`InscripcionPeriodoID`) REFERENCES `periodos` (`PeriodoID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Asignar Seccion` FOREIGN KEY (`InscripcionSeccionID`) REFERENCES `secciones` (`SeccionID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
